package sort;

import java.util.Arrays;

import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdRandom;

public class bubble {
	public static void sort() {
		int[] ages= {21,27,31,19,50,32,16,25};
		
		System.out.println(Arrays.toString(ages));
		//控制比较轮数
		for(int i=1;i<ages.length;i++) {
			//每轮比较多少
			for(int j=0;j<ages.length-i;j++) {
				if(ages[j]>ages[j+1]) {
					int tmp=0;
					tmp=ages[j];
					ages[j]=ages[j+1];
					ages[j+1]=tmp;					
				}
			}
		}
		System.out.println(Arrays.toString(ages));
	}
	public static void main(String[] args) {
        sort();
    }
}
