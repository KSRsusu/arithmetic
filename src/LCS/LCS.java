package LCS;
import java.util.ArrayList;  
import java.util.List;  
import java.util.Scanner;

public class LCS {
  
	static int dp[][]=new int [105][105];
	static int arr[][]=new int[105][105];
	//	动态规划
	 public static void f1(char arr1[],char arr2[],int n,int m) {
	        for(int i=1;i<=n;i++) {
	            for(int j=1;j<=m;j++) {
	                if(arr1[i-1]==arr2[j-1]) {
	                    dp[i][j]=dp[i-1][j-1]+1;
	                }else {
	                    dp[i][j]=Math.max(dp[i-1][j], dp[i][j-1]);
	                }
	            }
	        }
	        System.out.println(dp[n][m]);
	    }
	 //	备忘录
	public static int f2(char arr1[],char arr2[],int n,int m) {
		if(n==0||m==0) {
			return 0;
		}else if(arr1[n-1]==arr2[m-1]) {
			dp[n][m]=f2(arr1,arr2,n-1,m-1)+1;
		}else {
		
			 dp[n][m]=Math.max(f2(arr1,arr2,n-1,m),f2(arr1,arr2,n,m-1));}
		return dp[n][m];
	}
    public static void main(String[] args) {  
        Scanner cin = new Scanner(System.in);  
        while(cin.hasNext()) {
          String a=cin.next();
          String b=cin.next();
          char[] arr1=a.toCharArray();
          char[] arr2=b.toCharArray();
          System.out.println("----动态规划----");
          f1(arr1,arr2,arr1.length,arr2.length);
          System.out.println("----备忘录----");
          System.out.println(f2(arr1,arr2,arr1.length,arr2.length));       
        }       
    }  

}
