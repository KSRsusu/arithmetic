package MatrixChain;

public class MatrixChain {
	// 动态规划
	/**
	 * @name:matrixChain 求解最优值
	 * @param p: 矩阵维数信息数组
	 * @param m: 存放最优值数组, 上三角形式
	 * @param s: 存放分割位置下标的数组
	 * @return 返回最优值
	 **/
	public static int matrixChain(int n,int[] p, int[][] m, int[][] s) {
		for (int i = 1; i <= n; i++)
			// 本身为0
			m[i][i] = 0;  // 初始化二维数组
		for (int r = 2; r <= n; r++) {
			for (int i = 1; i <= n - r + 1; i++) { 
				int j = i + r - 1;
				// 先以i进行划分
				m[i][j] = m[i + 1][j] + p[i - 1] * p[i] * p[j];  // 求出Ai到Aj的连乘
				s[i][j] = i;  // 记录划分位置
				for (int k = i + 1; k < j; k++) {
					// 寻找是否有可优化的分割点
					int t = m[i][k] + m[k + 1][j] + p[i - 1] * p[k] * p[j];  // 公式
					if (t < m[i][j]) {
						m[i][j] = t;
						s[i][j] = k;
					}
				}
			}
		}
		return m[1][n];
	}
	
	/**
	 * @name: traceback 回溯最优解
	 * @param i、j: 连乘矩阵下标
	 * @param s: 存放分割位置下标的数组
	 * @output  最优计算次序
	 **/
	public static void traceback(int i, int j, int[][] s) {
		// 输出A[i:j] 的最优计算次序
		if (i == j) {
			// 递归出口
			System.out.print("A"+i);
			return;
		} else {
			System.out.print("(");
			// 递归输出左边
			traceback(i, s[i][j], s);
			// 递归输出右边
			traceback(s[i][j] + 1, j, s);
			System.out.print(")");
		}
	}
	// 备忘录
	/**
	 * @name: memoMatrix 备忘录法
	 * @param i、j: 连乘矩阵下标
	 * @param s: 存放分割位置下标的数组
	 * @output  最优计算次序
	 **/
	public static int memoMatrix(int i, int j, int[] p, int[][] m, int[][] s) {
		for (int x = 0; x < p.length; x++) {
            for (int y = 0; y < m[x].length; y++) {
                m[x][y] = -1;
            }
        }
        if (m[i][j] != -1)
            return m[i][j];
        if (i == j) {
            m[i][j] = 0;
            return 0;
        }
        int ans, minV = 9999999;
        //System.out.println("(" + i + "," + j + ")");//查看调用次序
        for (int k = i; k < j; k++) {
            ans = memoMatrix(k + 1, j, p, m, s) + memoMatrix(i, k, p, m, s) + p[i - 1] * p[k] * p[j];
            if (ans < minV) {
                minV = ans;
                s[i][j] = k;
            }
        }
        m[i][j] = minV;
        return minV;
    }
	
	public static void main(String[] args) {
		int[] p = new int[]{20, 13, 15, 3, 5,30};
		int[][] m = new int[p.length][p.length];
		int[][] s = new int[p.length][p.length];
		int n = p.length - 1;
		System.out.println("----动态规划法----");
		System.out.println("最优值为： "+matrixChain(n,p, m, s));
		System.out.print("最优计算次序：");
		traceback(1, p.length-1, s);
		System.out.println();
		System.out.println("----备忘录法----");
		System.out.println("最优值为： " + memoMatrix(1, n, p, m, s));
		System.out.print("最优计算次序：");
		traceback(1, p.length-1, s);
	}

	
	
}

